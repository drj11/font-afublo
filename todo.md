# Todo

- Shortern /quotesingle and /quotedbl
- alphabet balance and weight
- numbers balance and weight
- /two + bold?
- review under-/over-shoot (suggest round elements, /O, are kept
  as-is, and flat-topped elements are expanded vertically) /O
  has a vert range of 2086. Approximately 32 of over- and
  under-shoot gives a new cap-height of about 2048 to 2056.
  Maybe Cap-height 2040; range 2086 gives a symmetric overshoot
  of 11‰ of Cap Height.
- review /V

- review and smooth all angles

# END
