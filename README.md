# Afublo

The **O** is based on a wood-cut serif.
The rest of _this_ font is based on the O.

## Names

- Afublo (protoype)
- Omega VI
- Achonky

## Design notes

Design warning: 2520 UPM, cap-height 2000.

I will try and keep unqualified %s to be a percentage of the
cap-height (2000).

### O

The **O** is oblate, width 2350, and nominal height of 2000
(the cap-height).
Overshoots are (currently) 54 over (2.7% of cap-height) and 32
under (1.6%).

Stroke width is a shade over 30% (very thick):
31.3% for horizontal stroke. 30.0% to 30.4% for vertical stroke.


### E

E has already gone through a few iterations.
It is formed by taking the O,
sawing off the right-hand vertical stroke,
adding a middle crossbar,
pushing the corners to make them less rounded and more square.

E.789 is this E with the middle space divided vertically in the
ratio 7:8:9 (8 being the crossbar).

E.354 has the middle space divided 3:5:4. Compared to E.789 this
makes the middle crossbar thicker. Because the ratio of the
space around it has changed from 7:9 to 3:4, the middle crossbar
is closer to the top stroke, even though it's geometric centre
has not changed.

Compared to E.354, the current /E has thinned horizontals (to
make them 30.3%) and the gap:crossbar:gap ratios are 4:6:5
(actually a tiny bit more than 6).
The lengths of the 3 arms are a constant source of fiddling.


### U

**O** with the top part cut off and
the already vertical strokes extended up to the cap-line.
Terminated with lovingly soft rounded caps.

That version is as Wide As O and is a little bit too wide: `U.WAO`.

There is a less wide **U** that is as wide as **E**, square.
Or nearly so, it's actually 1% wider (2020 units), but because
of the overshoots, it's still slightly prolate.
That's `U.squareish`.

The current **U** is approximately halfway between `U.squareish`
and `U.WAO`.
`U.midi` is a slightly more accurate and slightly wider
approximation to the halfway position.


### J

Originally drawn as Wide As **O** and modelled on the **O**;
that version is `J.WAO`.
The current version is a narrower, more square version, which is
the same width as `U.squareish`.


### N

Originally drawn the same width as **H** (when /N was drawn; /H
is narrower now).
Uses the same vertical strokes as /H, modified slightly).
That version was unsatisfactory,
the current version is as wide as **O**.
The original version is `N.131` (131% of cap-height).


### M

Modelled on the **N**, the current **M** is
as wide as **O**.
Because the alphabet overall is very wide,
it is acceptable that
this **M** is only as wide as **N**.
A future revision may wish to consider wider **M**.

### /three

The 2024-10-02 drawing is wider than all the other numbers.
Consider making it mono and re-using this drawing in a `pnum`
feature.

### /one

Redrawn in 2024-10 to be narrower (and therefore non-mono
numbers); it's better.


# END
