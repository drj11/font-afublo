# Prospectus

Target: 205TF annual evaluation.


## Content

Omega VI is a design expanded from a single stolen **O**.
It is a bold poster-sized sans covering Latin script.

Currently the font is in a very early stage.
I have drawn a reasonable draft for the letters **A** to **Z**,
and only a small amount more:
the numbers **0**, **2**, **4**, **5**, **6**, **8**,
and two variants of ampersand.

The basic form of the alphabet is done, but a few letters,
**K** **X** **Y** **Z**, may need more than minor adjustments.
As a priority the basic latin alphabet will be completed with
**Æ** **Œ** **Ð** **Þ** and a reasonable set of marks.

For CubicType fonts the typical early targets for language are:
English, Icelandic, Maltese, Romanian, Slovenian, Welsh.

The primary stroke thickness is about 30% of cap-height, with
some secondary strokes at about 10% of cap-height.
There is some optical correction of horizontal and vertical
strokes (it is very small at about 1% of cap-height).

The thinner secondary strokes give some thick/thin contrast, but
not in the conventional sense.

From the **O** the design is based around a very wide and
somewhat rectangular oval.
The **O** is oblate (wider than high) with an outer contour that
pushes a circle almost into a rectangle.
The **O** has straight sides but a continuously curved top and
bottom, so it is more like a stretched racetrack than a
stretched circle.
This round-top straight-sides element is repeated and re-used
for **C** **G** **Q** and, perhaps more surprisingly, for **A**.

The **Q** has been drawn without descenders.

One of the design themes is wideness.
During the development of this font so far
this theme has been explored most in the letter **U**.
I would love to explore the use of the
[OpenType Layout `JSTF`
table](https://docs.microsoft.com/en-us/typography/opentype/spec/jstf)
to automatically use alternative glyphs of different widths to 
typeset justified text.

The work for the `JSTF` table will involve exploring different widths.
This could partially or wholly contribute to a monospace set,
at least for letters.
Either for trendy 2020 reasons or for less shocking vertical typesetting.

Ordinarily i am not a fan of Character Variant alternates for their own
sake, but i quite like both designs of ampersand so far.
The **Et** design suggests that a lowercase may be possible,
and while i am sure that a lowercase will be designed,
i am in no rush.
It would be reasonable to retail a design without a lowercase.


## The future

There will definitely be a future.

What lies in the future for this font?

The basics: **Ð** and other base latin glyphs.
Accent marks.
Letterspacing and kerning.
For this uppercase the design means
that relatively few pairs will need kerning, the generally
square features really help, and in particular the design of the
**A** and the **J**.

A monospace and a `JSTF` feature table have already been
mentioned.

I would like to explore the possibility of using the internal
space of the figure for some inlay/inline/decorative work.
Possibly with some colour layers.

The inline is an homage/rip-off of Paprika, Neuland Inline,
Yagi Double.

Fractions.

The name OMEGA VI features roman numerals,
so it seems that they should be in the font also.

# END
